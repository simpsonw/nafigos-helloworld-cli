# Nafigos Hello World CLI

This repository is used as a Hello World example for Nafigos using Argo Workflows. It demonstrates an Argo Workflow with Nafigos as well as a command-line based application being displayed on HTTP using a sidecar container

After running, you can see the Workflow output using curl or a web browser
